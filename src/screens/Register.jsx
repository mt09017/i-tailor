import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  Button,
  Pressable,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { colors } from "../utils/theme";

export default function Register() {


  function onFirsNameEntered(text) {
    console.log(text);
  }

  function onRegisterPress() {
    alert("asfhahfhasfhasfh");
  }

  return (
    <ScrollView>
      <View style={styles.formCon}>
        <TextInput
          placeholder={"First Name"}
          onChangeText={onFirsNameEntered}
          style={styles.inputCon}
        />
        <TextInput placeholder={"Second Name"} style={styles.inputCon} />
        <TextInput placeholder={"Email"} style={styles.inputCon} />
        <TextInput
          secureTextEntry={true}
          placeholder={"Password"}
          style={styles.inputCon}
        />

        <TouchableOpacity style={styles.btn} onPress={onRegisterPress}>
          <Text style={styles.btnText}>Register</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  formCon: {
    width: "100%",
    height: 200,
    marginTop: 200,
    padding: 10,
  },

  inputCon: {
    borderWidth: 1,
    borderColor: colors.primary,
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
  },

  btn: {
    padding: 10,
    borderRadius: 5,
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: colors.primary,
  },

  btnText: {
    color: colors.white,
  },
});
